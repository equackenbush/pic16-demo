#include <xc.h>

#pragma config WDTE = OFF	// turn off watchdog

void main(void)
{
	TRISAbits.TRISA2 = 0;	// configure RA2 as output

	while(1)
	{
		LATAbits.LATA2 = 0;	// turn LED on

		for (uint8_t i = 0; i < 10000; i++) {}

		LATAbits.LATA2 = 0;	// turn LED off
	}
}